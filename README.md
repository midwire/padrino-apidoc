# Padrino::Apidoc

A padrino sub-app plugin to generate API documentation for your controllers.

This code has no specs.  I rarely release, or write, code without them but I'm under a
deadline right now and don't have time.  I've just extracted this from a
project and put it into a gem. Hopefully someone can write specs for it and
help out.

## Installation

Add this line to your application's Gemfile:

    gem 'padrino-apidoc'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install padrino-apidoc

## Usage

### Templates

Copy the 'views' directory in this gem to your PADRINO_ROOT/config directory. Feel free to edit the HAML view templates as you see fit.

### Documentation Syntax

    ## Section Name

    # Create an application.
    #
    # @param <name>  the name of the application to create
    # @param [stack] the stack on which to create the application
    #
    # @request
    #   POST /apps.json
    #   name=example&stack=bamboo-ree-1.8.7
    # @response
    #   {
    #     "id":         1,
    #     "name":       "example",
    #     "owner":      "user@example.org",
    #     "created_at": "Sat Jan 01 00:00:00 UTC 2000",
    #     "stack":      "bamboo-ree-1.8.7",
    #     "slug_size":  1000000,
    #     "repo_size":  500000,
    #     "dynos":      1,
    #     "workers":    0
    #   }

### Mounting the Sub-App

At the end of your config/apps.rb file:

    Padrino.mount('Padrino::Apidoc::App').to('/docs')

### Viewing Docs

Navigate to 'localhost:9292/docs' or whatever hostname/port you are running on.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
