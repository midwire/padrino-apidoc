# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'padrino/apidoc/version'

Gem::Specification.new do |spec|
  spec.name          = "padrino-apidoc"
  spec.version       = Padrino::Apidoc::VERSION
  spec.authors       = ["Chris Blackburn"]
  spec.email         = ["chris@midwiretech.com"]
  spec.description   = %q{A padrino sub-app plugin to generate API documentation for your controllers.}
  spec.summary       = %q{A padrino sub-app plugin to generate API documentation for your controllers.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "pry-nav"
  spec.add_development_dependency "midwire_common", '~> 0.1.7'

  spec.add_dependency 'padrino'
end
