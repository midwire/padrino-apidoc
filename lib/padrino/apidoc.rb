require 'padrino'
require "padrino/apidoc/version"
require "padrino/apidoc/parser"

module Padrino
  module Apidoc

    class App < Padrino::Application
      register Padrino::Rendering
      register Padrino::Helpers

      helpers do
        def urlize_section(section)
          section.downcase.gsub(" ", "_")
        end

        def docs_in_section(docs, section)
          docs.select { |doc| urlize_section(doc[:section]) == section }
        end

        def code(code)
          Haml::Filters::Preserve.render(Haml::Filters::Escaped.render(code))
        end

        def doc(page, title)
          active = (env["PATH_INFO"] == "/#{page}") ? "active" : ""
          %{ <li class="#{active}"><a href="/#{page}">#{title}</a></li> }
        end
      end

      disable :sessions

      class << self
        def sections_in(docs)
          docs.map { |doc| doc[:section] }.uniq.compact
        end
      end

      set :docs, Parser.parse(Dir["#{Padrino.root}/app/controllers/*.rb"]).docs
      set :sections, App.sections_in(settings.docs)

      ########################################
      # ROUTES
      get "/" do
        render :index, layout_engine: :haml, layout: 'apidoc', :locals => {
          :docs     => settings.docs,
          :sections => settings.sections
        }
      end

      get "/docs.css" do
        content_type "text/css"
        sass :docs
      end

      get "/:section" do |section|
        render :section, layout_engine: :haml, layout: 'apidoc', :locals => {
          :docs     => docs_in_section(settings.docs, section),
          :sections => settings.sections,
          :section  => settings.sections.detect { |s| urlize_section(s) == section }
        }
      end
    end

  end
end
